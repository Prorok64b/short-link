# frozen_string_literal: true

password = 'Qwerty228'

user = FactoryBot.create(
  :user,
  password: password,
  password_confirmation: password
)

FactoryBot.create(:link, user: user, visits: FactoryBot.build_list(:visit, 20))
FactoryBot.create(:link, user: user, visits: FactoryBot.build_list(:visit, 15))
FactoryBot.create(:link, user: user, visits: FactoryBot.build_list(:visit, 10))
