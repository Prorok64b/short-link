# frozen_string_literal: true

class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.string :slug
      t.string :origin_url, index: true

      t.timestamps
    end

    add_index :links, :slug, unique: true
  end
end
