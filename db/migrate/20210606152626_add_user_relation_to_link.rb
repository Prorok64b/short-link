# frozen_string_literal: true

class AddUserRelationToLink < ActiveRecord::Migration[6.0]
  def change
    add_reference :links, :user, foreign_key: { on_delete: :cascade }
  end
end
