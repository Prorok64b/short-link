# frozen_string_literal: true

class CreateVisits < ActiveRecord::Migration[6.0]
  def change
    create_table :visits do |t|
      t.references :link, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end
