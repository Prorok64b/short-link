# Short Link

### Deps and database

```bash
$ bundle install
$ rails db:create db:migrate db:seed
```

## Specs
```bash
$ bundle exec rspec
```

## Usage

### Run server
```bash
$ rails s

default host -> localhost:3000
```

### Run Sidekiq
```bash
$ bundle exec sidekiq
```

### Sign Up
```
POST localhost:3000/auth

payload: {
  "email": "some@email.com",
  "password": "some-strong-pass",
  "password_confirmation": "some-strong-pass"
}
```

### Sign In
```
POST localhost:3000/auth/sign_in

payload: {
  "email": "some@email.com",
  "password": "some-strong-pass"
}

response headers:
- "access-token"
- "client"
- "uid"
```

### Create link
##### Request:
```
POST localhost:3000/api/v1/links

headers: {
    "access-token": "xxx",
    "client": "xxx",
    "uid": "xxx"
}

payload: {
    "origin_url": "http://awesome.com/""
}
```
##### Response:
```
response: {
    "id": 1,
    "slug": "27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "origin_url": "http://awesome.com/",
    "shorted_url": "http://localhost:3000/27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "created_at": "2021-06-08T17:00:19.403Z"
}
```

Field `shorted_url` contains shorted url, that leads to an `origin_url`.

### Update link
##### Request:
```
PATCH localhost:3000/api/v1/links/:id

headers: {
    "access-token": "xxx",
    "client": "xxx",
    "uid": "xxx"
}

payload: {
    "origin_url": "http://awesome2.com/""
}
```
##### Response:
```
response: {
    "id": 1,
    "slug": "27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "origin_url": "http://awesome2.com/",
    "shorted_url": "http://localhost:3000/27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "created_at": "2021-06-08T17:00:19.403Z"
}
```

### Get all links
##### Request:
```
GET localhost:3000/api/v1/links

headers: {
    "access-token": "xxx",
    "client": "xxx",
    "uid": "xxx"
}
```
##### Response:
```
response: [{
    "id": 1,
    "slug": "27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "origin_url": "http://awesome2.com/",
    "shorted_url": "http://localhost:3000/27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "created_at": "2021-06-08T17:00:19.403Z",
    "visits_count": 0
}]
```

### Get link
##### Request:
```
GET localhost:3000/api/v1/links/:id

headers: {
    "access-token": "xxx",
    "client": "xxx",
    "uid": "xxx"
}
```
##### Response:
```
response: {
    "id": 1,
    "slug": "27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "origin_url": "http://awesome2.com/",
    "shorted_url": "http://localhost:3000/27ce03f2-8af3-44e2-98ef-8e5d6add3330",
    "created_at": "2021-06-08T17:00:19.403Z",
    "visits_count": 0
}
```
