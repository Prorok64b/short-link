# frozen_string_literal: true

class Link < ApplicationRecord
  belongs_to :user

  has_many :visits

  validates :slug, uniqueness: true, presence: true
  validates :origin_url, presence: true, url: { no_local: true }
end
