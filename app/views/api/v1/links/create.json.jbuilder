# frozen_string_literal: true

json.partial! @link, as: :link
