# frozen_string_literal: true

json.id link.id
json.slug link.slug
json.origin_url link.origin_url
json.shorted_url resolver_url(slug: link.slug)
json.created_at link.created_at
