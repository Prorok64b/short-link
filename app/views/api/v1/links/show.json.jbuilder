# frozen_string_literal: true

json.partial! @link, as: :link
json.visits_count @link.visits_count
