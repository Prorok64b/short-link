# frozen_string_literal: true

json.array! @links do |link|
  json.partial! link, as: :link
  json.visits_count link.visits_count
end
