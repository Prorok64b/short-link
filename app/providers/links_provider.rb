# frozen_string_literal: true

class LinksProvider
  def initialize(scope: default_scope)
    @scope = scope
  end

  def call
    add_visit_counts

    @scope
  end

  private

  def add_visit_counts
    visit_counts = Visit.select('link_id', 'count(*) as total').group(:link_id)

    @scope = @scope.joins("LEFT JOIN (#{visit_counts.to_sql}) v ON v.link_id = links.id")
                 .select('links.*', 'v.total as visits_count')
  end

  def default_scope
    Link.all
  end
end
