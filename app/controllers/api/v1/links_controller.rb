# frozen_string_literal: true

module Api
  module V1
    class LinksController < RootController
      def index
        @links = link_scope
      end

      def show
        @link = find_link
      end

      def create
        @link = Interactions::Links::Create.run!(
          link_params.merge(user: current_user)
        )
      end

      def update
        @link = Interactions::Links::Update.run!(
          link: find_link,
          attrs: link_params
        )
      end

      def destroy
        Interactions::Links::Destroy.run!(
          link: find_link
        )
      end

      private

      def find_link
        link_scope.find(params[:id])
      end

      def link_scope
        LinksProvider.new(scope: policy_scope(Link)).call
      end

      def link_params
        params.permit(:origin_url)
      end
    end
  end
end
