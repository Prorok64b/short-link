# frozen_string_literal: true

module Api
  class RootController < ApplicationController
    include Pundit

    before_action :authenticate_user!

    rescue_from 'ActiveRecord::RecordInvalid' do |exception|
      msg = exception.record.errors.messages

      render json: { error: msg }, status: :unprocessable_entity
    end

    rescue_from 'ActiveInteraction::InvalidInteractionError' do |exception|
      msg = exception.interaction.errors.messages

      render json: { error: msg }, status: :unprocessable_entity
    end

    rescue_from 'ActiveRecord::RecordNotFound' do |_exception|
      render json: [], status: :not_found
    end
  end
end
