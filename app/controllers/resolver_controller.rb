# frozen_string_literal: true

class ResolverController < ActionController::API
  def index
    origin_url = SlugResolver.new(params[:slug]).call

    redirect_to origin_url
  rescue ActiveRecord::RecordNotFound
    render :not_found, status: :not_found
  end
end
