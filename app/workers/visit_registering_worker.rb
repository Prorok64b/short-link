# frozen_string_literal: true

class VisitRegisteringWorker
  include Sidekiq::Worker
  sidekiq_options queue: :default

  def perform(link_id)
    Interactions::Visits::Create.run!(link_id: link_id)
  end
end
