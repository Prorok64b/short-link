# frozen_string_literal: true

module Interactions
  module Visits
    class Create < ApplicationInteraction
      record :link_id, presence: true, class: Link

      alias link link_id

      def execute
        visit = factory

        visit.save!

        visit
      end

      private

      def factory
        Visit.new(link: link)
      end
    end
  end
end
