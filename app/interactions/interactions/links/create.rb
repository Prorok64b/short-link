# frozen_string_literal: true

module Interactions
  module Links
    class Create < ApplicationInteraction
      record :user, presence: true
      string :origin_url

      def execute
        link = factory

        link.save!

        link
      end

      private

      def factory
        slug = SlugGenerator.call

        Link.new(
          origin_url: origin_url,
          user: user,
          slug: slug
        )
      end
    end
  end
end
