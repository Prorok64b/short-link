# frozen_string_literal: true

module Interactions
  module Links
    class Destroy < ApplicationInteraction
      record :link, presence: true

      def execute
        link.destroy!
      end
    end
  end
end
