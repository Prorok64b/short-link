# frozen_string_literal: true

module Interactions
  module Links
    class Update < ApplicationInteraction
      record :link, presence: true

      hash :attrs do
        string :origin_url
      end

      def execute
        link.update!(attrs.compact)

        link
      end
    end
  end
end
