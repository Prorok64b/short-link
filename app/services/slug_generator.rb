# frozen_string_literal: true

class SlugGenerator
  def self.call
    SecureRandom.uuid
  end
end
