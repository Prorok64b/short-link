# frozen_string_literal: true

class SlugResolver
  def initialize(slug)
    @slug = slug
  end

  def call
    link = find_link

    register_visit(link)

    link.origin_url
  end

  private

  attr_reader :slug

  def find_link
    Link.find_by!(slug: slug)
  end

  def register_visit(link)
    VisitRegisteringWorker.perform_async(link.id)
  end
end
