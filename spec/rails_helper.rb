# frozen_string_literal: true

require 'simplecov'

SimpleCov.minimum_coverage 95
SimpleCov.minimum_coverage_by_file 80

unless ENV['TEST']
  SimpleCov.start 'rails' do
    add_filter '/spec'
    add_filter '/vendor'
    add_filter '/app/controllers/application_controller.rb'
    add_filter '/app/mailers/application_mailer.rb'
    add_filter '/app/policies/application_policy.rb'
    add_filter '/app/channels'
    add_filter '/app/helpers'
    add_filter '/app/jobs'
    add_filter '/tmp'
  end
end

require 'spec_helper'
require 'support/factory_bot'
require 'database_cleaner'
require 'sidekiq/testing'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)

abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'rspec/rails'

# Helpers
require_relative './helpers/authorization_helper'

Sidekiq::Testing.fake!

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end
RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
    DatabaseCleaner.strategy = :transaction
  end

  config.around do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
