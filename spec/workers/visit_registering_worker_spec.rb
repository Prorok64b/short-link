# frozen_string_literal: true

require 'rails_helper'

describe VisitRegisteringWorker, type: :worker do
  subject(:worker) { described_class.new.perform(link_id) }

  let!(:link) { create(:link) }

  context 'when given valid `link_id`' do
    let(:link_id) { link.id }

    it 'creates new record' do
      expect { worker }.to change(Visit, :count).from(0).to(1)
    end
  end

  context 'when given `link_id` as `nil`' do
    let(:link_id) { nil }

    it 'raises proper error and doesn\'t create new record' do
      expect { worker }.to raise_error(ActiveInteraction::InvalidInteractionError)
        .and change(Visit, :count).by(0)
    end
  end

  context 'when given unexisting `link_id`' do
    let(:link_id) { 99_999 }

    it 'raises proper error and doesn\'t create new record' do
      expect { worker }.to raise_error(ActiveInteraction::InvalidInteractionError)
        .and change(Visit, :count).by(0)
    end
  end
end
