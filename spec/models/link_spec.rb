# frozen_string_literal: true

require 'rails_helper'

describe Link, type: :model do
  it 'builds valid record with factory' do
    expect(build(:link)).to be_valid
  end
end
