# frozen_string_literal: true

require 'rails_helper'

describe User, type: :model do
  it 'builds valid record with factory' do
    expect(build(:user)).to be_valid
  end
end
