# frozen_string_literal: true

require 'rails_helper'

describe Visit, type: :model do
  it 'builds valid record with factory' do
    expect(build(:visit)).to be_valid
  end
end
