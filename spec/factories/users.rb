# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email  { Faker::Internet.email }
    password { 'Qwerty228#$@' }
    password_confirmation { 'Qwerty228#$@' }
  end
end
