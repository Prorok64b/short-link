# frozen_string_literal: true

FactoryBot.define do
  factory :visit do
    link
  end
end
