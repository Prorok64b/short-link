# frozen_string_literal: true

FactoryBot.define do
  factory :link do
    slug { SecureRandom.uuid }
    origin_url { 'http://awesome.com' }
    user
  end
end
