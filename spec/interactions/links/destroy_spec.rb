# frozen_string_literal: true

require 'rails_helper'

describe Interactions::Links::Destroy, type: :interaction do
  subject(:interaction) { described_class.run(params) }

  let!(:link) { create(:link) }

  context 'when given valid link record' do
    let(:params) { { link: link } }

    it 'makes interaction to be valid' do
      expect(interaction).to be_valid
    end

    it 'deletes a link record' do
      expect { interaction }.to change(Link, :count).by(-1)
    end
  end

  context 'when given `nil`' do
    let(:params) { { link: nil } }

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end

    it 'doesn\'t delete a link record' do
      expect { interaction }.to change(Link, :count).by(0)
    end
  end
end
