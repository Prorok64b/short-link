# frozen_string_literal: true

require 'rails_helper'

describe Interactions::Links::Create, type: :interaction do
  subject(:interaction) { described_class.run(params.merge(user: user)) }

  let!(:user) { create(:user) }

  context 'when given valid params' do
    let(:params) { { origin_url: 'https://some-awesome-site.com/some/path' } }

    it 'returns Link' do
      expect(interaction.result).to be_an_instance_of(Link)
    end

    it 'makes interaction to be valid' do
      expect(interaction).to be_valid
    end

    it 'creates new record' do
      expect { interaction }.to change(Link, :count).from(0).to(1)
    end
  end

  context 'when given empty params' do
    let(:params) { {} }

    it 'doesn\'t create new record' do
      expect { interaction }.not_to change(Link, :count).from(0)
    end

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end

    it 'returns empty result' do
      expect(interaction.result).to eq(nil)
    end

    it 'returns proper error message' do
      expect(interaction.errors.messages).to eq({ origin_url: ['is required'] })
    end
  end

  context 'when given `origin_url` as `nil`' do
    let(:params) { { origin_url: nil } }

    it 'doesn\'t create new record' do
      expect { interaction }.not_to change(Link, :count).from(0)
    end

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end

    it 'returns empty result' do
      expect(interaction.result).to eq(nil)
    end

    it 'returns proper error message' do
      expect(interaction.errors.messages).to eq({ origin_url: ['is required'] })
    end
  end

  context 'when given `origin_url` as empty string' do
    let(:params) { { origin_url: '' } }

    it 'raises proper exception and creates no record' do
      expect { interaction }.to raise_error(ActiveRecord::RecordInvalid)
        .and change(Link, :count).by(0)
    end
  end

  context 'when given invalid `origin_url`' do
    let(:params) { { origin_url: 'definitely-invalid-url' } }

    it 'raises proper exception and creates no record' do
      expect { interaction }.to raise_error(ActiveRecord::RecordInvalid)
        .and change(Link, :count).by(0)
    end
  end

  context 'when given `origin_url` as local address' do
    let(:params) { { origin_url: 'localhost:3000' } }

    it 'raises proper exception and creates no record' do
      expect { interaction }.to raise_error(ActiveRecord::RecordInvalid)
        .and change(Link, :count).by(0)
    end
  end
end
