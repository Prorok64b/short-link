# frozen_string_literal: true

require 'rails_helper'

describe Interactions::Links::Update, type: :interaction do
  subject(:interaction) { described_class.run(params) }

  let(:origin_url) { 'https://google.com' }
  let!(:link) { create(:link, origin_url: origin_url) }

  context 'when given valid params' do
    let(:params) do
      {
        link: link.id,
        attrs: { origin_url: 'https://some-awesome-site.com/some/path' }
      }
    end

    it 'makes interaction to be valid' do
      expect(interaction).to be_valid
    end

    it 'returns record with updated `origin_url`' do
      expect(interaction.result.origin_url).to eq(params[:attrs][:origin_url])
    end
  end

  context 'when given empty params' do
    let(:params) { {} }

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end

    it 'doesn\'t change `origin_url`' do
      expect { interaction }.not_to change(link.reload, :origin_url).from(origin_url)
    end
  end

  context 'when given empty `attrs`' do
    let(:params) { { link: link.id, attrs: {} } }

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end

    it 'doesn\'t change `origin_url`' do
      expect { interaction }.not_to change(link.reload, :origin_url).from(origin_url)
    end
  end

  context 'when given `origin_url` as `nil`' do
    let(:params) { { link: link.id, attrs: { origin_url: nil } } }

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end

    it 'doesn\'t change `origin_url`' do
      expect { interaction }.not_to change(link.reload, :origin_url).from(origin_url)
    end
  end

  context 'when given non-existing `link`' do
    let(:params) do
      {
        link: 99_999,
        attrs: { origin_url: 'https://some-awesome-site.com/some/path' }
      }
    end

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end

    it 'doesn\'t change `origin_url`' do
      expect { interaction }.not_to change(link.reload, :origin_url).from(origin_url)
    end
  end

  context 'when given `origin_url` as empty string' do
    let(:params) { { link: link.id, attrs: { origin_url: '' } } }

    it 'raises proper exception' do
      expect { interaction }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'when given invalid `origin_url`' do
    let(:params) { { link: link.id, attrs: { origin_url: 'invalid-str' } } }

    it 'raises proper exception' do
      expect { interaction }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'when given `origin_url` as local address' do
    let(:params) { { link: link.id, attrs: { origin_url: 'localhost:80' } } }

    it 'raises proper exception' do
      expect { interaction }.to raise_error(ActiveRecord::RecordInvalid)
    end
  end
end
