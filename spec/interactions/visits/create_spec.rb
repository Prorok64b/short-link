# frozen_string_literal: true

require 'rails_helper'

describe Interactions::Visits::Create, type: :interaction do
  subject(:interaction) { described_class.run(params) }

  let!(:link) { create(:link) }

  context 'when given valid params' do
    let(:params) { { link_id: link.id } }

    it 'returns Link' do
      expect(interaction.result).to be_an_instance_of(Visit)
    end

    it 'makes interaction to be valid' do
      expect(interaction).to be_valid
    end

    it 'creates new record' do
      expect { interaction }.to change(Visit, :count).from(0).to(1)
    end
  end

  context 'when given `link_id` as `nil`' do
    let(:params) { { link_id: nil } }

    it 'doesn\'t create new record' do
      expect { interaction }.not_to change(Visit, :count).from(0)
    end

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end
  end

  context 'when given unexisting `link_id`' do
    let(:params) { { link_id: 99_999 } }

    it 'doesn\'t create new record' do
      expect { interaction }.not_to change(Visit, :count).from(0)
    end

    it 'makes interaction to be invalid' do
      expect(interaction).not_to be_valid
    end
  end
end
