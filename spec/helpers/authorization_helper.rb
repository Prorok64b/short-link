# frozen_string_literal: true

module AuthorizationHelper
  def auth_tokens_for_user(user, password)
    post '/auth/sign_in/',
         params: {
           email: user.email,
           password: password
         },
         as: :json

    response.headers.slice('client', 'access-token', 'uid')
  end
end
