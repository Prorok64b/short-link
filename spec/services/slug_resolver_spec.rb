# frozen_string_literal: true

require 'rails_helper'

describe SlugResolver, type: :service do
  subject(:service) { described_class.new(slug).call }

  let(:origin_url) { 'http://awesome.com' }
  let!(:link) { create(:link, origin_url: origin_url) }
  let(:slug) { link.slug }

  context 'when given existing `slug`' do
    it 'returns proper `origin_url`' do
      expect(service).to eq(origin_url)
    end

    it 'runs VisitRegisteringWorker' do
      expect { service }.to change(VisitRegisteringWorker.jobs, :size).by(1)
    end
  end

  context 'when given un-exsting `slug`' do
    let(:slug) { 'some-unknown-slug' }

    it 'raises proper exception' do
      expect { service }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'doesn\'t run VisitRegisteringWorker' do
      expect { service }.to raise_error(ActiveRecord::RecordNotFound)
        .and change(VisitRegisteringWorker.jobs, :size).by(0)
    end
  end
end
