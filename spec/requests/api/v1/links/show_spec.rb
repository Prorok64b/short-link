# frozen_string_literal: true

require 'rails_helper'

describe 'GET /api/v1/links/:id', type: :request do
  include AuthorizationHelper

  subject(:request) do
    get "/api/v1/links/#{id}", headers: auth_tokens_for_user(user, password)
    response
  end

  let(:password) { '123SomeStringPass#$%^&' }
  let!(:user) { create(:user, password: password, password_confirmation: password) }

  let!(:link) { create(:link, user: user) }
  let(:id) { link.id }

  context 'when sent valid `id`' do
    before { create_list(:visit, 10, link: link) }

    it 'returns proper http status' do
      expect(request.status).to eq(200)
    end

    it 'returns response body with `id` value' do
      expect(request.parsed_body['id']).to eq(id)
    end

    it 'returns response body with `slug` value' do
      expect(request.parsed_body['slug']).to eq(link.slug)
    end

    it 'returns response body with `origin_url` value' do
      expect(request.parsed_body['origin_url']).to eq(link.origin_url)
    end

    it 'returns response body with `shorted_url` value' do
      expect(request.parsed_body['shorted_url']).to eq("http://www.example.com/#{link.slug}")
    end

    it 'returns response body with `created_at` value' do
      expect(request.parsed_body['created_at']).to be_an_instance_of(String)
    end

    it 'returns response body with `visits_count` value' do
      expect(request.parsed_body['visits_count']).to eq(10)
    end
  end

  context 'when sent non-existing `id`' do
    let(:id) { 99_999 }

    it 'returns proper http status' do
      expect(request.status).to eq(404)
    end
  end

  context 'when `link` belongs to another user' do
    let(:link) { create(:link) }

    it 'returns proper http status' do
      expect(request.status).to eq(404)
    end
  end
end
