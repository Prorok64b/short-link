# frozen_string_literal: true

require 'rails_helper'

describe 'POST /api/v1/links', type: :request do
  include AuthorizationHelper

  subject(:request) do
    post '/api/v1/links', params: params, headers: auth_tokens_for_user(user, password)
    response
  end

  let(:password) { '123SomeStringPass#$%^&' }
  let!(:user) { create(:user, password: password, password_confirmation: password) }

  context 'when sent valid params' do
    let(:params) { { origin_url: 'https://awesome-site.com/some/path' } }

    it 'returns proper http status' do
      expect(request.status).to eq(200)
    end

    it 'creates new record' do
      expect { request }.to change(Link, :count).by(1)
    end

    it 'returns response body with `id` value' do
      expect(request.parsed_body['id']).to be_an_instance_of(Integer)
    end

    it 'returns response body with `slug` value' do
      expect(request.parsed_body['slug']).to be_an_instance_of(String)
    end

    it 'returns response body with `origin_url` value' do
      expect(request.parsed_body['origin_url']).to eq(params[:origin_url])
    end

    it 'returns response body with `shorted_url` value' do
      expect(request.parsed_body['shorted_url']).to be_an_instance_of(String)
    end

    it 'returns response body with `created_at` value' do
      expect(request.parsed_body['created_at']).to be_an_instance_of(String)
    end
  end

  context 'when sent empty params' do
    let(:params) { {} }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'creates no new records' do
      expect { request }.to change(Link, :count).by(0)
    end

    it 'returns response body with proper errors' do
      expect(request.parsed_body).to eq({ 'error' => { 'origin_url' => ['is required'] } })
    end
  end

  context 'when sent `origin_url` as `nil`' do
    let(:params) { { origin_url: nil } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'creates no new records' do
      expect { request }.to change(Link, :count).by(0)
    end

    it 'returns response body with proper errors' do
      expect(request.parsed_body).to eq({ 'error' => { 'origin_url' => ['is required'] } })
    end
  end

  context 'when sent `origin_url` as empty string' do
    let(:params) { { origin_url: '' } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'creates no new records' do
      expect { request }.to change(Link, :count).by(0)
    end

    it 'returns response body with proper errors' do
      expect(request.parsed_body).to eq(
        { 'error' => { 'origin_url' => ['can\'t be blank', 'is not a valid URL'] } }
      )
    end
  end

  context 'when sent invalid `origin_url`' do
    let(:params) { { origin_url: 'some not url string' } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'creates no new records' do
      expect { request }.to change(Link, :count).by(0)
    end

    it 'returns response body with proper errors' do
      expect(request.parsed_body).to eq({ 'error' => { 'origin_url' => ['is not a valid URL'] } })
    end
  end

  context 'when sent `origin_url` as local address' do
    let(:params) { { origin_url: 'http://localhost:4000' } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'creates no new records' do
      expect { request }.to change(Link, :count).by(0)
    end

    it 'returns response body with proper errors' do
      expect(request.parsed_body).to eq({ 'error' => { 'origin_url' => ['is not a valid URL'] } })
    end
  end
end
