# frozen_string_literal: true

require 'rails_helper'

describe 'GET /api/v1/links', type: :request do
  include AuthorizationHelper

  subject(:request) do
    get '/api/v1/links', headers: auth_tokens_for_user(user, password)
    response
  end

  let(:password) { '123SomeStringPass#$%^&' }
  let!(:user) { create(:user, password: password, password_confirmation: password) }

  before do
    create_list(:link, 5, visits: build_list(:visit, 3))
  end

  context 'when current_user has 3 links' do
    let!(:users_links) { create_list(:link, 3, user: user, visits: build_list(:visit, 3)) }

    it 'returns proper http status' do
      expect(request.status).to eq(200)
    end

    it 'returns response body with 3 links' do
      expect(request.parsed_body.size).to eq(3)
    end

    it 'returns response body with proper ids' do
      ids = request.parsed_body.map { |link| link['id'] }

      users_links.each do |link|
        expect(ids).to include(link.id)
      end
    end

    it 'returns response body with proper visits_counts' do
      visits_counts = request.parsed_body.map { |link| link['visits_count'] }

      users_links.each do |_link|
        expect(visits_counts).to include(3)
      end
    end
  end

  context 'when current_user has no links' do
    it 'returns proper http status' do
      expect(request.status).to eq(200)
    end

    it 'returns empty response body' do
      expect(request.parsed_body.size).to eq(0)
    end
  end
end
