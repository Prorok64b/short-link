# frozen_string_literal: true

require 'rails_helper'

describe 'DELETE /api/v1/links/:id', type: :request do
  include AuthorizationHelper

  subject(:request) do
    delete "/api/v1/links/#{id}", headers: auth_tokens_for_user(user, password)
    response
  end

  let(:password) { '123SomeStringPass#$%^&' }
  let!(:user) { create(:user, password: password, password_confirmation: password) }

  let!(:link) { create(:link, user: user) }
  let(:id) { link.id }

  context 'when sent valid `id`' do
    it 'returns proper http status' do
      expect(request.status).to eq(204)
    end
  end

  context 'when sent non-existing `id`' do
    let(:id) { 99_999 }

    it 'returns proper http status' do
      expect(request.status).to eq(404)
    end
  end

  context 'when `link` belongs to another user' do
    let(:link) { create(:link) }

    it 'returns proper http status' do
      expect(request.status).to eq(404)
    end
  end
end
