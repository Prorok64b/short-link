# frozen_string_literal: true

require 'rails_helper'

describe 'PATCH /api/v1/links/:id', type: :request do
  include AuthorizationHelper

  subject(:request) do
    patch "/api/v1/links/#{id}", params: params, headers: auth_tokens_for_user(user, password)
    response
  end

  let(:password) { '123SomeStringPass#$%^&' }
  let!(:user) { create(:user, password: password, password_confirmation: password) }

  let!(:link) { create(:link, user: user, origin_url: 'https://google.com') }
  let(:id) { link.id }

  context 'when sent valid params' do
    let(:params) do
      {
        origin_url: 'https://awesome-site.com/some/path'
      }
    end

    it 'returns proper http status' do
      expect(request.status).to eq(200)
    end

    it 'returns response body with `id` value' do
      expect(request.parsed_body['id']).to eq(id)
    end

    it 'returns response body with `slug` value' do
      expect(request.parsed_body['slug']).to eq(link.slug)
    end

    it 'returns response body with `origin_url` value' do
      expect(request.parsed_body['origin_url']).to eq(params[:origin_url])
    end

    it 'returns response body with `shorted_url` value' do
      expect(request.parsed_body['shorted_url']).to eq("http://www.example.com/#{link.slug}")
    end

    it 'returns response body with `created_at` value' do
      expect(request.parsed_body['created_at']).to be_an_instance_of(String)
    end
  end

  context 'when sent empty params' do
    let(:params) { {} }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'doesn\'t change `origin_url`' do
      expect { request }.not_to change(link.reload, :origin_url).from('https://google.com')
    end
  end

  context 'when sent `origin_url` as `nil`' do
    let(:params) { { origin_url: nil } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'doesn\'t change `origin_url`' do
      expect { request }.not_to change(link.reload, :origin_url).from('https://google.com')
    end
  end

  context 'when sent `origin_url` as empty string' do
    let(:params) { { origin_url: '' } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'doesn\'t change `origin_url`' do
      expect { request }.not_to change(link.reload, :origin_url).from('https://google.com')
    end
  end

  context 'when sent invalid `origin_url`' do
    let(:params) { { origin_url: 'some not url string' } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'doesn\'t change `origin_url`' do
      expect { request }.not_to change(link.reload, :origin_url).from('https://google.com')
    end
  end

  context 'when sent `origin_url` as local address' do
    let(:params) { { origin_url: 'http://localhost:4000' } }

    it 'returns proper http status' do
      expect(request.status).to eq(422)
    end

    it 'doesn\'t change `origin_url`' do
      expect { request }.not_to change(link.reload, :origin_url).from('https://google.com')
    end
  end

  context 'when sent non-existing `id`' do
    let(:id) { 99_999 }
    let(:params) do
      {
        origin_url: 'https://awesome-site.com/some/path'
      }
    end

    it 'returns proper http status' do
      expect(request.status).to eq(404)
    end

    it 'doesn\'t change `origin_url`' do
      expect { request }.not_to change(link.reload, :origin_url).from('https://google.com')
    end
  end

  context 'when `link` belongs to another user' do
    let(:link) { create(:link, origin_url: 'https://google.com') }
    let(:params) do
      {
        origin_url: 'https://awesome-site.com/some/path'
      }
    end

    it 'returns proper http status' do
      expect(request.status).to eq(404)
    end

    it 'doesn\'t change `origin_url`' do
      expect { request }.not_to change(link.reload, :origin_url).from('https://google.com')
    end
  end
end
