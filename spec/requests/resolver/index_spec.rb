# frozen_string_literal: true

require 'rails_helper'

describe 'GET /:slug', type: :request do
  subject(:request) { get "/#{slug}" }

  let!(:link) { create(:link, origin_url: 'http://awesome.com') }
  let(:slug) { link.slug }

  context 'when sent proper `slug`' do
    it 'performs redirect' do
      expect(request).to redirect_to('http://awesome.com')
    end
  end

  context 'when sent un-known `slug`' do
    before { request }

    let(:slug) { 'something-unknown' }

    it 'returns proper http status' do
      expect(response.status).to eq(404)
    end
  end
end
