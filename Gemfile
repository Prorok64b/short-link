# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.7'

gem 'active_interaction', '~> 4.0', '>= 4.0.1'

gem 'devise_token_auth'

gem 'jbuilder', '~> 2.7'

gem 'rails', '~> 6.0.3', '>= 6.0.3.7'
gem 'sqlite3', '~> 1.4'

gem 'puma', '~> 4.1'

gem 'bootsnap', '>= 1.4.2', require: false

gem 'pundit'

gem 'sidekiq', '~> 6.2', '>= 6.2.1'

gem 'validate_url'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]

  gem 'factory_bot'
  gem 'factory_bot_rails'

  gem 'faker', git: 'https://github.com/faker-ruby/faker.git', branch: 'master'

  gem 'rspec-rails', '~> 5.0.0'

  gem 'rubocop', '~> 1.16', require: false
  gem 'rubocop-performance', '~> 1.11', '>= 1.11.3'
  gem 'rubocop-rails', '~> 2.10', '>= 2.10.1'
  gem 'rubocop-rspec', require: false
end

group :development do
  gem 'database_cleaner'
  gem 'listen', '~> 3.2'

  gem 'simplecov', require: false

  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
